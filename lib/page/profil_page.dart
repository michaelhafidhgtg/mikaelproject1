import 'package:flutter/material.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Profile'),
      ),
      body: Column(
        children: [
          costumForm("Nama", "Masukan Namane Bapakmu", "Nama Harus Di Ganti", Icons.person),
          costumForm("Nomor Hp", "Masukan ID EPEP Bapakmu", "Nomor Hp Harus Di Ganti", Icons.phone),
          costumForm("Email", "Masukan Email e Bapakmu", "Email Harus Di Ganti", Icons.email),
          costumForm("Password", "Password Salah Kebangeten", "Password Harus Di Ganti", Icons.lock,),
        ],
      ),
    );
  }

  Padding costumForm(String labelText,String hintText, String helperText, IconData icon, ) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        decoration: InputDecoration(
          hintText: hintText,
          labelText: labelText,
          helperText: helperText,
          prefixIcon: Icon(icon),
          suffixIcon: Icon(Icons.chevron_right),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12.0),
            borderSide: BorderSide(width: 2, color: Colors.black),
          ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(12.0),
                borderSide: BorderSide(
                    width: 2, color: Colors.blue),
            ),
          ),
        );
  }
}